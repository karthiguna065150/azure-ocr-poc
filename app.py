from azure.cognitiveservices.vision.computervision import ComputerVisionClient
from azure.cognitiveservices.vision.computervision.models import OperationStatusCodes
from azure.cognitiveservices.vision.computervision.models import VisualFeatureTypes
from msrest.authentication import CognitiveServicesCredentials
from smart_open import open

import os
import time
import json
import azure
from flask import Flask, request

subscription_key = os.getenv("SUBKEY")
endpoint = os.getenv("END_POINT")
connect_str = os.getenv("CONNECT_STR")

transport_params = {
    'client': azure.storage.blob.BlobServiceClient.from_connection_string(connect_str),
}

client = ComputerVisionClient(
    endpoint, CognitiveServicesCredentials(subscription_key))

app = Flask("ocr_app")
app.config["JSON_SORT_KEYS"] = False


@app.route('/healthCheck')
def healthcheck():
    return {"status": "Azure ocr app is running"}


@app.route("/sync", methods=["post"])
def sync_handler():
    obj = request.get_json()
    if obj["blob_type"] == "private":
        with open(obj["file_uri"], "rb", transport_params=transport_params)as content:
            res = client.recognize_printed_text_in_stream(
                image=content, raw=True)
    else:
        res = client.recognize_printed_text(obj["file_uri"])
    return res.response.json()


@app.route('/async', methods=["POST"])
def async_handler():
    obj = request.get_json()
    file_uri = obj.get("file_uri")
    blob_type = obj.get("blob_type", "public")

    operation_id = create_job(file_uri, blob_type)
    read_result = check_job_status(operation_id)

    result = read_result.analyze_result
    res = result.as_dict()
    response = parse_response(res, obj)
    return response


def create_job(file_uri, blob_type):
    print(blob_type)
    if blob_type == "private":
        print("reading file")
        with open(file_uri, "rb", transport_params=transport_params)as content:
            read_response = client.read_in_stream(
                image=content, raw=True)
        print("completed")
    else:
        read_response = client.read(url=file_uri, raw=True)
    read_operation_location = read_response.headers["Operation-Location"]
    operation_id = read_operation_location.split("/")[-1]
    return operation_id


def check_job_status(operation_id):
    while True:
        read_result = client.get_read_result(operation_id)
        if read_result.status not in ['notStarted', 'running']:
            break
        time.sleep(1)
    return read_result


def parse_response(res, obj):
    file_uri = obj.get("file_uri")
    dest_uri = form_dest_uri(file_uri, obj.get("destination_uri"))
    create_file_with_text_only = obj.get("create_file_with_text_only", True)
    include_ocr_text = obj.get("include_oct_text", False)
    include_bounding_box = obj.get("include_oct_text", False)
    include_confidence = obj.get("include_confidence", False)

    if create_file_with_text_only:
        pages = res["read_results"]
        response = {"extract_text": "\n".join(
            [line["text"] for page in pages for line in page["lines"]])}
        upload_result(response, dest_uri)
        return response

    if any([include_ocr_text, include_confidence, include_bounding_box]):
        pass

    else:
        return res


def upload_result(res, dest_uri):
    with open(dest_uri, "w", transport_params=transport_params)as f:
        json.dump(res, f)
    return {"status": "success"}


def form_dest_uri(file_uri, dest_uri):
    base_name = os.path.basename(file_uri).split(".")[0] + ".json"
    dest_uri = os.path.join(dest_uri, base_name)
    print(dest_uri)
    return dest_uri



if __name__ == "__main__":
    app.run(debug=True)
